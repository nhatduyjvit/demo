<?php

namespace App;

class Post
{
    /**
     * @return array
     */
    public function get(){
        return $this->readData();
    }

    /**
     * Read data
     *
     * @return array
     */
    protected function readData(){
        $csvPath =  __DIR__ .'/data/post.csv';
        $csvFile = file($csvPath);
        $data = array_slice($csvFile, 1);
        foreach ($data as $key => $value)
        {
            $arrayValue = str_getcsv($value);
            $data[$key] = [
                'id' => $arrayValue[0],
                'title' => $arrayValue[1],
                'content' => $arrayValue[2]
            ];
        }
        return $data;
    }
}
