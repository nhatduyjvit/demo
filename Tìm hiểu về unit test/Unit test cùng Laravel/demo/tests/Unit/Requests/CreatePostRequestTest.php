<?php

namespace Tests\Unit\Requests;

use Faker\Factory;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;
use App\Http\Requests\CreatePostRequest;

class CreatePostRequestTest extends TestCase
{
    protected $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    /**
     * @test
     * @dataProvider  provideData
     */
    public function test_create_post_request($data, $expect)
    {
        // Validate data
        $request = new CreatePostRequest();
        $isValid =  Validator::make($data, $request->rules())->passes();

        // Assert validated
        $this->assertSame($isValid, $expect);
    }

    /**
     * @return array
     */
    public function provideData()
    {
        yield
        'title_is_empty' =>
        [
            $this->baseData( ['title' => '']),
            false
        ];

        yield
        'content_is_empty' =>
        [
            $this->baseData( ['content' => '']),
            false
        ];

        yield
        'request_with_empty_data' =>
        [
            $this->baseData( ['title' => '', 'content' => '']),
            false
        ];

        yield
        'valid_data' =>
        [
            $this->baseData(),
            true
        ];
    }


    /**
     * @param array $replacements
     * @return array
     */
    protected function baseData (array $replacements =[]) {
        return array_replace(
            [
                'title' => $this->faker->text(90),
                'content' => $this->faker->text(90)
            ],
            $replacements
        );
    }
}
