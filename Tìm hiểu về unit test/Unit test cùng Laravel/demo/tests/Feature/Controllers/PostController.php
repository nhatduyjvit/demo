<?php

namespace Tests\Feature\Controllers;

use App\Models\Post;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Tests\TestCase;

class PostController extends TestCase
{
    use RefreshDatabase;

    protected $faker;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->faker = Factory::create();
    }

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        Post::factory(10)->create();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('/api/posts');
        $response->assertOk();
        $response->assertJsonCount(10);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_show()
    {
        // Get first post
        $indexResponse = $this->get('/api/posts');
        $post = $indexResponse->baseResponse->getOriginalContent()->first();


        // Call and assert response
        $showResponse = $this->get("/api/posts/{$post->id}");

        $showResponse->assertOk();
        $showResponse->assertJsonStructure([
            'id',
            'title',
            'content'
        ]);
        $showResponse->assertSeeInOrder([
            'id' => $post->id
        ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_store()
    {
        // Prepare data
        $postData = [
            'title' => $this->faker->text(90),
            'content' => $this->faker->text(90)
        ];


        // Call and assert response
        $response = $this->postJson("/api/posts", $postData);

        $response->assertCreated();
        $response->assertSeeInOrder([
            'title' => $postData['title'],
            'content' => $postData['content']
        ]);
        $this->assertDatabaseHas(Post::class, $postData);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_edit()
    {
        // Get first post
        $indexResponse = $this->get('/api/posts');
        $post = $indexResponse->baseResponse->getOriginalContent()->first()->toArray();

        // Prepare data
        $data = array_merge($post,[
            'title' => $this->faker->text(90),
            'content' => $this->faker->text(90)
        ]);

        // Call and assert response
        $response = $this->putJson("/api/posts/{$post['id']}", $data);

        $response->assertNoContent();
        $this->assertDatabaseHas(Post::class, Arr::only($data, ['id', 'title', 'content']));
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_delete()
    {
        // Get first post
        $indexResponse = $this->get('/api/posts');
        $post = $indexResponse->baseResponse->getOriginalContent()->first();


        // Call and assert response
        $response = $this->delete("/api/posts/{$post->id}");

        $response->assertNoContent();
        $this->assertDatabaseMissing(Post::class, Arr::only($post->toArray(), ['id', 'title', 'content']));
    }
}
